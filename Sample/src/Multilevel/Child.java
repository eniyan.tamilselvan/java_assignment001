package Multilevel;

public class Child extends Parent{  //multilevel
	private void clgId() {
		System.out.println("clf id: 12");
	}
	private void clgName() {
		System.out.println("clgName:VIT");
	}
	public static void main(String[] args) {
		Child c=new Child();
		c.clgId();
		c.clgName();
		c.student();
		c.name();
		c.tester();
		c.tester2();
		Parent p =new Parent();
		p.student();
		p.name();
		p.tester();
		p.tester2();
		Gparent g=new Gparent();
		g.tester();
		g.tester2();
		
	}
}