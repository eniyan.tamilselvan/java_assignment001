package org.base.main;

import java.util.Scanner;

public class PalanDemo {

	public static void main(java.lang.String[] args) {
				
				Scanner sc = new Scanner(System.in);
				System.out.println("Enter the string");
				String str1 = sc.nextLine();
				
				StringBuffer sb1 = new StringBuffer(str1);
				sb1.reverse();
				String str2=sb1.toString();
				if (str1.equals(str2)) {
					System.out.println("Given is palindrome");
					
				} else {
					System.out.println("Given is not palindrome");	
				
			}

		}

}
